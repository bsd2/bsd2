-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: localhost    Database: obid
-- ------------------------------------------------------
-- Server version	5.7.12-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auction`
--

DROP TABLE IF EXISTS `auction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auction` (
  `ID_Auction` int(11) NOT NULL AUTO_INCREMENT,
  `Start` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `End` datetime NOT NULL,
  `Total_Bayar` double NOT NULL,
  `Terbayar` double NOT NULL,
  `Sisa_Bayar` double NOT NULL,
  `Status` int(11) NOT NULL,
  PRIMARY KEY (`ID_Auction`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auction`
--

LOCK TABLES `auction` WRITE;
/*!40000 ALTER TABLE `auction` DISABLE KEYS */;
/*!40000 ALTER TABLE `auction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bidding`
--

DROP TABLE IF EXISTS `bidding`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bidding` (
  `id_bidding` int(11) NOT NULL AUTO_INCREMENT,
  `id_item` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `tgl_bidding` datetime DEFAULT CURRENT_TIMESTAMP,
  `jml_bidding` float DEFAULT NULL,
  PRIMARY KEY (`id_bidding`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bidding`
--

LOCK TABLES `bidding` WRITE;
/*!40000 ALTER TABLE `bidding` DISABLE KEYS */;
INSERT INTO `bidding` VALUES (1,1,1,'2016-12-24 00:00:00',4000000),(4,1,3,'2016-12-23 20:28:05',4400000),(5,1,3,'2016-12-23 20:45:31',4841000);
/*!40000 ALTER TABLE `bidding` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cart`
--

DROP TABLE IF EXISTS `cart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cart` (
  `id_cart` int(11) NOT NULL AUTO_INCREMENT,
  `id_item` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `price` float DEFAULT NULL,
  PRIMARY KEY (`id_cart`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cart`
--

LOCK TABLES `cart` WRITE;
/*!40000 ALTER TABLE `cart` DISABLE KEYS */;
INSERT INTO `cart` VALUES (1,1,1,1,5000),(2,4,3,1,1000),(3,3,3,1,1000000);
/*!40000 ALTER TABLE `cart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `d_auction`
--

DROP TABLE IF EXISTS `d_auction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `d_auction` (
  `ID_Auction` int(11) NOT NULL,
  `User_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID_Auction`,`User_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `d_auction`
--

LOCK TABLES `d_auction` WRITE;
/*!40000 ALTER TABLE `d_auction` DISABLE KEYS */;
/*!40000 ALTER TABLE `d_auction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `d_pembayaran`
--

DROP TABLE IF EXISTS `d_pembayaran`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `d_pembayaran` (
  `ID_Auction` int(11) NOT NULL,
  `ID_Pembayaran` int(11) NOT NULL,
  PRIMARY KEY (`ID_Auction`,`ID_Pembayaran`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `d_pembayaran`
--

LOCK TABLES `d_pembayaran` WRITE;
/*!40000 ALTER TABLE `d_pembayaran` DISABLE KEYS */;
/*!40000 ALTER TABLE `d_pembayaran` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item`
--

DROP TABLE IF EXISTS `item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item` (
  `ID_Item` int(11) NOT NULL AUTO_INCREMENT,
  `Nama` varchar(1024) NOT NULL,
  `Keterangan` varchar(1024) NOT NULL,
  `Harga_Max` double NOT NULL,
  `Harga_Min` double NOT NULL,
  `Aktif` tinyint(4) NOT NULL,
  `ID_Kategori` int(11) NOT NULL,
  `ID_User` int(11) NOT NULL,
  `tgl_akhir` datetime NOT NULL,
  PRIMARY KEY (`ID_Item`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item`
--

LOCK TABLES `item` WRITE;
/*!40000 ALTER TABLE `item` DISABLE KEYS */;
INSERT INTO `item` VALUES (1,'ASUS','INI ASUS',5000000,3000000,1,1,1,'2016-12-24 00:00:00'),(2,'ACER','INI ACER',4000000,3000000,1,1,1,'2016-12-27 00:00:00'),(3,'XIAMI','XIAMI TIPE AJAJAJAJ',1000000,400000,1,1,1,'2016-12-28 00:00:00'),(4,'OM TELOLET OM','tjukTAE',1000,10,1,1,3,'2016-12-25 17:46:00');
/*!40000 ALTER TABLE `item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_auction`
--

DROP TABLE IF EXISTS `item_auction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_auction` (
  `ID_Item` int(11) NOT NULL,
  `ID_Auction` int(11) NOT NULL,
  PRIMARY KEY (`ID_Item`,`ID_Auction`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_auction`
--

LOCK TABLES `item_auction` WRITE;
/*!40000 ALTER TABLE `item_auction` DISABLE KEYS */;
/*!40000 ALTER TABLE `item_auction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `item_image`
--

DROP TABLE IF EXISTS `item_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `item_image` (
  `ID_item_image` int(11) NOT NULL AUTO_INCREMENT,
  `ID_item` int(11) DEFAULT NULL,
  `namepath` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ID_item_image`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `item_image`
--

LOCK TABLES `item_image` WRITE;
/*!40000 ALTER TABLE `item_image` DISABLE KEYS */;
INSERT INTO `item_image` VALUES (1,1,'1.png'),(2,1,'2.jpg'),(3,2,'4.jpg'),(4,2,'6.png'),(5,3,'5.jpg'),(6,4,'product-81.jpg'),(7,4,'product-82.jpg'),(8,4,'product-83.jpg'),(9,4,'product-84.jpg'),(10,4,'product-85.jpg');
/*!40000 ALTER TABLE `item_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kategori`
--

DROP TABLE IF EXISTS `kategori`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kategori` (
  `ID_Kategori` int(11) NOT NULL AUTO_INCREMENT,
  `Nama` varchar(1024) NOT NULL,
  `Aktif` tinyint(4) NOT NULL,
  `path` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ID_Kategori`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kategori`
--

LOCK TABLES `kategori` WRITE;
/*!40000 ALTER TABLE `kategori` DISABLE KEYS */;
INSERT INTO `kategori` VALUES (1,'SMART PHONE',1,'assets/globals/'),(2,'HOUSE',1,'assets/globals/');
/*!40000 ALTER TABLE `kategori` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pembayaran`
--

DROP TABLE IF EXISTS `pembayaran`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pembayaran` (
  `ID_Pembayaran` int(11) NOT NULL AUTO_INCREMENT,
  `Tgl_Pembayaran` datetime NOT NULL,
  `Total_Pembayaran` double NOT NULL,
  PRIMARY KEY (`ID_Pembayaran`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pembayaran`
--

LOCK TABLES `pembayaran` WRITE;
/*!40000 ALTER TABLE `pembayaran` DISABLE KEYS */;
/*!40000 ALTER TABLE `pembayaran` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rate_user`
--

DROP TABLE IF EXISTS `rate_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rate_user` (
  `ID_User_Terima` int(11) NOT NULL,
  `ID_User_Beri` int(11) NOT NULL,
  `Komentar` text NOT NULL,
  PRIMARY KEY (`ID_User_Terima`,`ID_User_Beri`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rate_user`
--

LOCK TABLES `rate_user` WRITE;
/*!40000 ALTER TABLE `rate_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `rate_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `ID_User` int(11) NOT NULL AUTO_INCREMENT,
  `Nama` varchar(1024) NOT NULL,
  `Email` varchar(1024) NOT NULL,
  `Password` varchar(1024) NOT NULL,
  `Status` tinyint(4) NOT NULL DEFAULT '1',
  `validasi` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`ID_User`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'SA','sa@obid.com','12345',1,1),(3,'Yulius','yulius@gmail.com','$2y$09$BQawMPtS/C6SsaVXxd2rA.u1YN8ScgSWzz0EddRniV8H/lAuqbaSa',1,1),(4,'Nama','email@1.com','$2y$09$cQI89ZDz8tk0kp8/4HZw7.nRheGupRjuQvlbNNXiLfibfOUCwje9m',1,0);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_bayar`
--

DROP TABLE IF EXISTS `user_bayar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_bayar` (
  `ID_User` int(11) NOT NULL,
  `ID_Pembayaran` int(11) NOT NULL,
  PRIMARY KEY (`ID_User`,`ID_Pembayaran`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_bayar`
--

LOCK TABLES `user_bayar` WRITE;
/*!40000 ALTER TABLE `user_bayar` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_bayar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_item`
--

DROP TABLE IF EXISTS `user_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_item` (
  `ID_User` int(11) NOT NULL,
  `ID_Item` int(11) NOT NULL,
  PRIMARY KEY (`ID_User`,`ID_Item`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_item`
--

LOCK TABLES `user_item` WRITE;
/*!40000 ALTER TABLE `user_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_rating_item`
--

DROP TABLE IF EXISTS `user_rating_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_rating_item` (
  `id_user_rating_item` int(11) NOT NULL AUTO_INCREMENT,
  `id_item` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `star` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_user_rating_item`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_rating_item`
--

LOCK TABLES `user_rating_item` WRITE;
/*!40000 ALTER TABLE `user_rating_item` DISABLE KEYS */;
INSERT INTO `user_rating_item` VALUES (1,1,1,4),(2,1,2,3);
/*!40000 ALTER TABLE `user_rating_item` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-12-27 22:05:32
