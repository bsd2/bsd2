<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HomePage extends CI_Controller {
	public function __construct()
  {
      parent::__construct();
      $this->load->library('session');
  }
	public function Menu($a='home')
	{
    $param = array ('page' => $a);
		$this->load->view('global/head');
		$this->load->view('group/default/index',$param);
		$this->load->view('group/default/includes/popup');
	}
	public function Listitem($a)
	{
    $param = array ('kategori'=>$a);
		$this->load->view('global/head');
		$this->load->view('group/default/index',$param);
		$this->load->view('group/default/includes/popup');
	}
	public function Detail($a,$b)
	{
		// $items['data'] = $this->GetListDetail($a,$b);
		// print_r($items['data']);
	  $param = array ('kategori'=>$a,'item'=>$b,'data'=>$this->GetListDetail($a,$b));

		// print_r($param);exit();

		$this->load->view('global/head');
		$this->load->view('group/default/index',$param);
		$this->load->view('group/default/includes/popup');
	}
  public function UserLogin(){
    $this->load->model('HomeModel');
    echo $this->HomeModel->UserLoginModel();
  }
  public function UserRegister(){
    $this->load->model('HomeModel');
    echo $this->HomeModel->UserRegisterModel();
  }
  public function UserLogout(){
      $this->session->unset_userdata('useremail');
  }
	function GetKatalog()
	{
		 $this->load->model('HomeModel');
		 echo $this->HomeModel->GetKatalogModel();
	}
	function GetKatalogCount()
	{
		 $this->load->model('HomeModel');
		 echo $this->HomeModel->GetKatalogCountModel();
	}
	function GetListKatalog()
	{
		 $this->load->model('HomeModel');
		 echo $this->HomeModel->GetKatalogListModel();
	}
	function GetListDetail($a,$b)
	{
		 $this->load->model('HomeModel');
		 return $this->HomeModel->GetDetailListModel($a,$b);
	}
	function AddToCart()
	{
		 $this->load->model('HomeModel');
		 echo $this->HomeModel->AddToCartModel();
	}
	function RemoveFromCart()
	{
		 $this->load->model('HomeModel');
		 echo $this->HomeModel->RemoveFromCartModel();
	 }
	function GetFromCart()
	{
		 $this->load->model('HomeModel');
		 echo $this->HomeModel->GetFromCartModel();
	}
	function GetPopUpBid()
	{
		$this->load->model('HomeModel');
		echo $this->HomeModel->GetPopUpBidModel();
	}
	function AddNewItem()
	{
     $this->load->model('HomeModel');
     echo $this->HomeModel->AddNewItemModel();
	}
	function UserConfirm($a)
	{
     $this->load->model('HomeModel');
     echo $this->HomeModel->UserConfirmModel($a);
		 $this->output->set_header('refresh:3; url='.base_url());
	}
	public function Pembayaran()
	{
		$this->load->model('HomeModel');
		$param = array ('payment'=>'payment','data'=>$this->HomeModel->GetListPaymentModel());
		$this->load->view('global/head');
		$this->load->view('group/default/index',$param);
		$this->load->view('group/default/includes/popup');
	}
	function AddToBidding()
	{
     $this->load->model('HomeModel');
     echo $this->HomeModel->AddToBiddingModel();
	}

	function SearchQuery()
	{
		 $this->load->model('HomeModel');
		 echo $this->HomeModel->SearchQueryModel();
	}
	function GetHotItem()
	{
		 $this->load->model('HomeModel');
		 echo $this->HomeModel->GetHotItemModel();
	}
	function GetSnipItem()
	{
		 $this->load->model('HomeModel');
		 echo $this->HomeModel->GetSnipItemModel();
	}
}
