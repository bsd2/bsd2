<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminPage extends CI_Controller {
	public function index()
	{
		$this->load->view('includes/head.php');
		$this->load->view('index.php');
		$this->load->view('includes/popup.php');
	}
	public function listitem()
	{
		$this->load->view('includes/head.php');
		$this->load->view('menus/listitem.php');
		$this->load->view('includes/popup.php');
	}
	public function detailitem()
	{
		$this->load->view('includes/head.php');
		$this->load->view('menus/detailitem.php');
		$this->load->view('includes/popup.php');
	}

}
