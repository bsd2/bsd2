<?php
class HomeModel extends CI_Model{
    function __construct() {
        parent::__construct();
        $this->load->helper(array('url','file'));
    }
    function HashPassword($password)
    {
        $options = [
            'cost' => 9,
        ];
        return password_hash($password, PASSWORD_BCRYPT, $options);
    }

    function UserLoginModel(){
        $query=$this->db->select("id_user,password,email")->where(array('email' => $this->input->post('emaillogin'),'validasi'=>'1'))->limit("1")->get('user')->row_array();
        if (password_verify($this->input->post('passlogin'),$query['password'])==1)
        {
            $this->session->set_userdata('useremail',$query['email']);
            return "LOGGED IN";
        }
        else {
            return "FAILED";
        }
    }
    function UserRegisterModel()
    {
        $query='INSERT INTO user (nama, password, email,validasi) VALUES  ("'. $this->input->post('nameregister').'","'.$this->HashPassword( $this->input->post('passregister')).'","'. $this->input->post('emailregister').'","0")';
        $this->db->query($query);
        $subject = "Obid Konfirmasi";
        $from['email']="no-reply@obid.com";
        $from['name']="OBID";
        $to[$this->input->post('emailregister')]=$this->input->post('emailregister');
        $content="<html>
        </html>
        <i>mohon buka halaman berikut untuk melakukan validasi</i>
        <a href='http://www.localhost.com/auction/validasi/".$this->input->post('emailregister')."'>Link Di Sini</a>
        </html>";
        $this->SendEmailModel($subject,$from,$to,$content);
        return "REGISTER BERHASIL, MOHON MELAKUKAN KONFIRMASI EMAIL TERLEBIH DAHULU SEBELUM LOGIN";
        $this->db->query($query);
    }
    function UserConfirmModel($email)
    {
        $querys=$this->db->select("id_user")->where(array('email' => $email))->limit("1")->get('user')->row_array();
        $query="UPDATE user SET validasi=1 WHERE id_user = '".$querys['id_user']."'";
        $this->db->query($query);
        return "KONFIRMASI EMAIL ".$email." BERHASIL DILAKUKAN HALAMAN AKAN OTOMATIS TER-REDERECT KE HALAMAN UTAMA DALAM 3 DETIK";

    }
    function GetKatalogModel()
    {
        $value;
        $katalog="SELECT id_kategori,nama from kategori WHERE aktif=1";
        foreach ($this->db->query($katalog)->result() as $row)
        {
           $hasil['id_kategori']=str_replace(" ","-",strtolower($row->nama));
           $hasil['nama']=strtoupper($row->nama);
           $value[]=$hasil;
        }
        return json_encode($value);
    }
    function GetKatalogCountModel()
    {
        $value;
        $katalog="SELECT round((count(i.id_item)+20)/20,0) `total`
        from kategori k,item i
        where k.id_kategori = i.id_kategori
        and k.nama like '%".str_replace("-","%",$this->input->post('kategori'))."%'";
        return $this->db->query($katalog)->row()->total;
    }
    function GetDetailListModel($kategori,$barang)
    {
        $detail="select k.id_kategori,k.nama `nama_kategori`,i.nama `nama_item`,i.keterangan,i.id_item,im.id_item_image,i.Harga_Max,i.Harga_Min,concat(k.path,k.id_kategori,'/',i.ID_Item,'/',im.namepath) `truepath`,r.star,r.reviews,i.tgl_akhir
        from kategori k,item i left join (select round(avg(r.star),0) star,count(r.id_item) reviews,r.id_item  from user_rating_item r group by r.id_item) r  on r.id_item = i.id_item , item_image im
        where k.id_kategori = i.id_kategori
        and im.id_item = i.id_item
        and i.nama like '".str_replace("-","%",$barang)."'
        and k.nama like '".str_replace("-","%",$kategori)."'";
        $value['nama_item']=$this->db->query($detail)->row()->nama_item;
        $value['keterangan']=$this->db->query($detail)->row()->keterangan;
        $value['id_item']=$this->db->query($detail)->row()->id_item;
        $value['harga_max']=$this->db->query($detail)->row()->Harga_Max;
        $value['harga_min']=$this->db->query($detail)->row()->Harga_Min;
        $value['nama_kategori']=$this->db->query($detail)->row()->nama_kategori;
        $value['tgl_akhir']=$this->db->query($detail)->row()->tgl_akhir;
        $value['star']=$this->db->query($detail)->row()->star;
        $value['reviews']=$this->db->query($detail)->row()->reviews;
        foreach ($this->db->query($detail)->result() as $row)
        {
             $hasil['src']=$row->truepath;
             $hasil['id_detail_barang']=$row->id_item_image;
             $mini[]=$hasil;
        }
        $value['detailnya']=$mini;
        return $value;
    }
    function GetKatalogListModel()
    {
        $page=$this->input->post('page');
        $page=($page-1)*20;
        $katagori = $this->input->post('kategori');


        if(str_replace("-"," ",$katagori)=="")
        {
            $katagori='%';
        }
        $barang="select k.id_kategori,k.nama `nama_kategori`,i.nama `nama_item`,i.keterangan,i.Harga_Max,i.Harga_Min,concat(k.path,k.id_kategori,'/',i.Id_Item,'/',max(im.namepath)) `truepath`,i.Id_Item,i.tgl_akhir
        from kategori k,item i, item_image im
        where k.id_kategori = i.id_kategori
        and im.id_item = i.id_item
        and k.nama like '%".str_replace("-","%",$katagori)."%'
        group by k.id_kategori,k.nama,i.nama,i.keterangan,i.harga_max,i.harga_min,k.path,i.Id_Item,i.tgl_akhir
        LIMIT ".$page.", 20";
        // return $barang;
        $value="";
        foreach ($this->db->query($barang)->result() as $row)
        {

            $value.='<div class="col-sm-4 col-md-3">
                <div class="product-shortcode style-1">
                <div class="title">
                  <div class="simple-article size-1 color col-xs-b5"><a href="'.$this->config->item('base_url').'listitem/'.str_replace(' ','-',strtolower($row->nama_kategori)).'">'.strtoupper($row->nama_kategori).'</a></div>
                  <div class="h6 animate-to-green">
                  <a href="'.$this->config->item('base_url').'listitem/'.str_replace(' ','-',strtolower($row->nama_kategori)).'">'.strtolower($row->nama_item).'</a></div>
                </div>

                <div style="border: 1px #eee solid; padding: 5px;">
                <div class="simple-article size-1 col-xs-b5">SISA WAKTU : </div>
                <div class="countdown" data-end="'.$row->tgl_akhir.'"></div>
                </div>
                <div class="preview">
                <img src="'.$this->config->item('base_url').$row->truepath.'"/>
                  <div class="preview-buttons valign-middle">
                    <div class="valign-middle-content">
                      <a class="button size-2 style-2" href="'.$this->config->item('base_url').'listitem/'.str_replace(' ','-',strtolower($row->nama_kategori)).'/'.str_replace(' ','-',strtolower($row->nama_item)).'">
                        <span class="button-wrapper">
                              <span class="icon fa fa-arrow-right" style="color:white"></span>
                        <span class="text">lihat detail</span>
                        </span>
                      </a>
                      <a class="button size-2 style-3 open-popup itms" data-rel="3" id="bidnow" aidi="'.$row->Id_Item.'" price="'.$row->Harga_Max.'">
                      <span class="button-wrapper">
                            <span class="icon fa fa-legal"></span>
                      <span class="text">mulai bid</span>
                      </span>
                      </a>
                    </div>
                  </div>
                </div>
                <div class="price">
                  <div class="simple-article size-1"> Harga Awal : '.strtolower($row->Harga_Min).'</div>
                  <div class="simple-article size-1"> Harga Beli : '.strtolower($row->Harga_Max).'</div>
                </div>
                <div class="description">
                  <div class="simple-article text size-2">'.strtolower($row->keterangan).'
                </div>

            </div>
        </div>
    </div>';
        }
        return $value;
    }
    function AddToCartModel(){
        $query=$this->db->select("id_user")->where(array('email' => $this->session->userdata('useremail')))->limit("1")->get('user')->row_array();
        $query='INSERT INTO cart (id_item,id_user,qty,price) VALUES  ("'. $this->input->post('id').'","'.$query['id_user'].'","1","'.$this->input->post('pric').'")';
        $this->db->query($query);
        return "Item Added";
    }

    function RemoveFromCartModel(){
        $query=$this->db->select("id_user")->where(array('email' => $this->session->userdata('useremail')))->limit("1")->get('user')->row_array();
        // return $this->input->post('itm');
        $query='DELETE FROM cart WHERE id_item ="'.$this->input->post('itm').'" AND id_user = "'.$query['id_user'].'"';
        $this->db->query($query);
        return "Item Removed";
    }
	function GetFromCartModel()
	{
        $user=$this->db->select("id_user")->where(array('email' => $this->session->userdata('useremail')))->limit("1")->get('user')->row_array();


        $query ="select i.id_item,c.qty,c.price,c.subtotal,i.nama,i.keterangan,concat(k.path,k.id_kategori,'/',i.Id_Item,'/',max(im.namepath)) `truepath`,cc.*,u.email
        from (select sum(c.qty)`qty`,max(price) `price`,sum(c.qty)*max(price) `subtotal`,c.id_item,c.id_user
        from cart c
        group by c.id_item,c.id_user) c, item i, kategori k,item_image im,(select sum(qty*price) `totalbayar`,count(*) `scount`,id_user from cart group by id_user) cc,user u
        where i.id_item = c.id_item
        and k.id_kategori = i.id_kategori
        and im.id_item = i.id_item
        and cc.id_user = c.id_user
        and u.id_user = c.id_user
        and c.id_user = '".$user['id_user']."'
        group by cc.id_user,i.id_item,i.nama,i.keterangan,u.email";

        $head='<b class="hidden-xs">'.$this->session->userdata('useremail').'</b>
        <span class="cart-icon">
            <i class="fa fa-shopping-bag" aria-hidden="true"></i>
            <span class="cart-label">0</span>
        </span>
        <span class="cart-title hidden-xs">RP 0</span>
        </a>';
        if($this->db->query($query)->num_rows() > 0)
        {

        $head='<b class="hidden-xs">'.$this->db->query($query)->row()->email.'</b>
        <span class="cart-icon">
            <i class="fa fa-shopping-bag" aria-hidden="true"></i>
            <span class="cart-label">'.$this->db->query($query)->row()->scount.'</span>
        </span>
        <span class="cart-title hidden-xs">RP '.$this->db->query($query)->row()->totalbayar.'</span>
        </a>
        <div class="cart-toggle hidden-xs hidden-sm">
        <div class="cart-overflow">';
        $deta='';

        foreach ($this->db->query($query)->result() as $row)
        {
            $deta.='<div class="cart-entry clearfix">
            <a class="cart-entry-thumbnail" href="#"><img style="width:76px;height=76px;" src="'.$this->config->item('base_url').$row->truepath.'" alt="" /></a>
            <div class="cart-entry-description">
            <table>
            <tr>
            <td>
            <div class="h6"><a href="#">'.$row->nama.'</a></div>
            <div class="simple-article size-1">QUANTITY: '.$row->qty.'</div>
            </td>
            <td>
            <div class="simple-article size-3 grey">'.$row->price.'</div>
            <div class="simple-article size-1">TOTAL: '.$row->subtotal.'</div>
            </td>
            <td>
            <div class="button-close clci" usr="'.$user['id_user'].'" itm="'.$row->id_item.'"></div>
            </td>
            </tr>
            </table>
            </div>
            </div>';
        };
        $foot='</div><div class="empty-space col-xs-b40"></div>
        <div class="row">
        <div class="col-xs-6">
          <div class="cell-view empty-space col-xs-b50">
            <div class="simple-article size-5 grey">TOTAL <span class="color"> RP '.$this->db->query($query)->row()->totalbayar.'</span></div>
          </div>
        </div>
        <div class="col-xs-6 text-right">
          <a class="button size-2 style-3" href="'.$this->config->item('base_url').'pembayaran'.'">
            <span class="button-wrapper">
                                        <span class="icon"><img src="'.$this->config->item('base_url').'assets/img/icon-4.png" alt=""></span>
            <span class="text">proceed to checkout</span>
            </span>
          </a>
        </div>
        </div>
        </div>';
        return $head.$deta.$foot;
        }
        return $head;
	}
  function GetPopUpBidModel()
  {
      $query = "select i.id_item,i.nama,i.keterangan,i.harga_max,i.harga_min,i.tgl_akhir,b.jml_bidding,b.tgl_bidding,k.nama `nama_kategori`,concat(k.path,k.id_kategori,'/',i.Id_Item,'/',max(im.namepath)) `truepath`
      from kategori k,item_image im,item i left join (select id_item,jml_bidding,tgl_bidding from bidding order by id_bidding desc limit 1) b on i.id_item = b.id_item
      where k.id_kategori = i.id_kategori
      and im.id_item = i.id_item
      and i.id_item = '".$this->input->post('id')."'";
      $value;
      foreach ($this->db->query($query)->result() as $row)
      {
         $hasil['nama_kategori']=$row->nama_kategori;
        $hasil['nama']=$row->nama;
         $hasil['id_item']=$row->id_item;
        $hasil['harga_max']=$row->harga_max;
       $hasil['harga_min']=$row->harga_min;
         if($row->jml_bidding==0){
            $hasil['jml_bidding']=$row->harga_min;
            $hasil['auto_bid']=$row->harga_min*1.1;
        }else {
           $hasil['jml_bidding']=$row->jml_bidding;
           $hasil['auto_bid']=$row->jml_bidding*1.1;
       }
     $hasil['tgl_bidding']=$row->tgl_bidding;
     $hasil['tgl_akhir']=$row->tgl_akhir;
    $hasil['path']='url('.$this->config->item('base_url').$row->truepath.')';
     $value[]=$hasil;
      }
     return json_encode($value);
  }

  function AddNewItemModel()
  {
      $query="SELECT AUTO_INCREMENT FROM information_schema.TABLES WHERE TABLE_SCHEMA=DATABASE() AND TABLE_NAME='item'";
      $kode =$this->db->query($query)->row()->AUTO_INCREMENT;


        $user =$this->db->select("id_user")->where(array('email' => $this->session->userdata('useremail')))->limit("1")->get('user')->row_array();
        $kategori =$this->db->select("id_kategori")->where(array('nama' => str_replace("-"," ",$this->input->post('kategori'))))->limit("1")->get('kategori')->row_array();
        $date= date("Y-m-d H:i:s" ,strtotime($this->input->post('tgl_akhir'))); //strtotime($this->input->post('tgl_akhir'));
        $querys="INSERT INTO item (Nama, Keterangan, Harga_Max, Harga_Min, Aktif, ID_Kategori, ID_User, tgl_akhir) VALUES ('".$this->input->post('product')."', '".$this->input->post('deskripsi')."', '".$this->input->post('hargamax')."', '".$this->input->post('hargamin')."', '1', '".$kategori['id_kategori']."', '".$user['id_user']."', '".$date."')";

        // return $querys;
      $this->db->query($querys);
      $query1="SELECT concat(k.path,k.id_kategori,'/',i.id_item) `path`
        from kategori k,item i
        where k.id_kategori = i.id_kategori
        and i.id_item = '".$kode."'";
      $pathfile = $this->db->query($query1)->row()->path;
      mkdir($pathfile);

     $this->load->library('upload');
     $files = $_FILES;
     $cpt = count($_FILES['userfile']['name']);
     $error="";
     for($i=0; $i<$cpt; $i++)
     {
         $_FILES['userfile']['name']= $files['userfile']['name'][$i];
         $_FILES['userfile']['type']= $files['userfile']['type'][$i];
         $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
         $_FILES['userfile']['error']= $files['userfile']['error'][$i];
         $_FILES['userfile']['size']= $files['userfile']['size'][$i];
         $this->upload->initialize($this->set_upload_options($pathfile));
         if (!$this->upload->do_upload())
         {
             $error .= array('error' => $this->upload->display_errors());
         }
         else {
             $this->AddItemImageModel($kode,$_FILES['userfile']['name']);
             $error = "SUKSES";
         }
     }
     return $error;
  }
  private function set_upload_options($path)
  {
     //upload an image options
     $config = array();
     $config['upload_path'] = $path;
     $config['allowed_types'] = '*';
     return $config;
 }

 function AddItemImageModel($item,$type)
 {
     $query='INSERT INTO item_image (id_item,namepath) VALUES  ("'.$item.'","'.str_replace(" ","_",$type).'")';
     $this->db->query($query);
 }
 function SendEmailModel($subject,$from,$to,$content)
 {
     $mail = new PHPMailer;
     $mail->SMTPDebug  = 0;
     $mail->SMTPAuth   = true;
     $mail->SMTPSecure = "ssl";
     $mail->From = $from['email'];
     $mail->FromName = $from['name'];
     $mail->CharSet = 'UTF-8';
     foreach ($to as $key => $value) {
         $mail->addBCC($value);
     }
     $mail->isHTML(true);
     $mail->Subject = $subject;
     $mail->msgHTML( $content );
     $mail->IsMail();
     $mail->send();
 }

 function GetListPaymentModel()
 {
     $query=$this->db->select("id_user")->where(array('email' => $this->session->userdata('useremail')))->limit("1")->get('user')->row_array();
     $value;
     $detail="select i.id_item,c.qty,c.price,c.subtotal,i.nama,i.keterangan,concat(k.path,k.id_kategori,'/',i.Id_Item,'/',max(im.namepath)) `truepath`,cc.*,u.email
    from (select sum(c.qty)`qty`,max(price) `price`,sum(c.qty)*max(price) `subtotal`,c.id_item,c.id_user
    from cart c
    group by c.id_item,c.id_user) c, item i, kategori k,item_image im,(select sum(qty*price) `totalbayar`,count(*) `scount`,id_user from cart group by id_user) cc,user u
    where i.id_item = c.id_item
    and k.id_kategori = i.id_kategori
    and im.id_item = i.id_item
    and cc.id_user = c.id_user
    and u.id_user = c.id_user
    and c.id_user = '".$query['id_user']."'
    group by cc.id_user,i.id_item,i.nama,i.keterangan,u.email";

    $value['totalbayar']=$this->db->query($detail)->row()->totalbayar;

     foreach ($this->db->query($detail)->result() as $row)
     {
        $hasil['id_item']=$row->id_item;
        $hasil['qty']=$row->qty;
        $hasil['price']=$row->price;
        $hasil['subtotal']=$row->subtotal;
        $hasil['nama']=$row->nama;
        $hasil['keterangan']=$row->keterangan;
        $hasil['truepath']=$this->config->item('base_url').$row->truepath;
        $hasil['scount']=$row->scount;
        $hasil['id_user']=$row->id_user;
        $mini[]=$hasil;
     }
     $value['detailnya']=$mini;
     return $value;
 }
 function AddToBiddingModel(){
     $query=$this->db->select("id_user")->where(array('email' => $this->session->userdata('useremail')))->limit("1")->get('user')->row_array();
     $query="INSERT INTO bidding (id_user,id_item,jml_bidding)
     VALUES ('".$query['id_user']."','".$this->input->post('id')."','".$this->input->post('pric')."')";
     $this->db->query($query);
     return "Bidding Telah dilakukan";
 }

 function SearchQueryModel()
 {
     $val;
     $query="SELECT k.id_kategori,k.nama `nama_kategori`,i.id_item `id_barang`,i.nama `nama_barang`,i.keterangan `deskripsi`,concat(k.path,k.id_kategori,'/',i.id_Item,'/',im.namepath) `path`
    from kategori k,item i, (select max(namepath)`namepath`,id_item from item_image im group by id_item) im
    where k.id_kategori = i.id_kategori
    and im.id_item = i.id_item
    AND (i.nama like '%".str_replace(" ","%",$this->input->post('query'))."%' or i.keterangan like '%".str_replace(" ","%",$this->input->post('query'))."%' or k.nama like '%".str_replace(" ","%",$this->input->post('query'))."%');";
     if($this->db->query($query)->num_rows()>0)
     {
         foreach ($this->db->query($query)->result() as $row)
         {
             $val[$row->nama_kategori][] = array
             (
                 // 'nama_kategori'   =>$row->nama_kategori,
             'nama_barang'    =>$row->nama_barang,
             'deskripsi'    =>$row->deskripsi,
             'path'    => $this->config->item('base_url').$row->path,
             'ref'    =>$this->config->item('base_url').'listitem/'.str_replace(' ','-',strtolower($row->nama_kategori)).'/'.str_replace(' ','-',strtolower($row->nama_barang))
              );
         }
     }
     else {
         return '0';
     }
     $re="";
     foreach ($val as $key => $value) {
        //  return $key;
        //  exit();
         $re.='<div class="row col-xs-b25">
         <h4>'.$key.'</h4>';
         foreach ($value as $key1 => $value1) {
             $re.='<div class="result-search-popup-entry clearfix">
             <a class="result-search-popup-entry-thumbnail" href="#">
             <img src="'.$value1['path'].'"/></a>
             <div class="result-search-popup-entry-description">
             <table>
             <tr>
               <td>
                 <div class="h6"><a href="'.$value1['ref'].'">'.$value1['nama_barang'].'</a></div>
                 <div class="simple-article size-1">'.$value1['deskripsi'].'</div>
               </td>
             </tr>
             </table>
             </div>
             </div>';
         }
         $re.='</div>';
         // return print_r($value);
     }
     return $re;
 }
 function GetHotItemModel()
 {
     $barang="SELECT k.id_kategori,k.nama `nama_kategori`,i.id_item `id_barang`,i.nama `nama_barang`,i.keterangan `deskripsi`,concat(k.path,k.id_kategori,'/',i.id_Item,'/',im.namepath) `path`,i.harga_max,i.harga_min,i.tgl_akhir
        from kategori k,item i, (select max(namepath)`namepath`,id_item from item_image im group by id_item) im
        where k.id_kategori = i.id_kategori
        and im.id_item = i.id_item";
     $value="";

    //  <div style="border: 1px #eee solid; padding: 5px;">
    //  <div class="simple-article size-1 col-xs-b5">SISA WAKTU : </div>
    //  <div class="countdown" data-end="'.$row->tgl_akhir.'"></div>
    //  </div>
     foreach ($this->db->query($barang)->result() as $row)
     {
         $value.='<div class="swiper-slide">
         <div class="product-shortcode style-1 big">
             <div class="product-label red">ONLY RP '.$row->harga_min.'</div>
             <div class="preview">
                 <img style="width:200px;height:200px;"src="'.$this->config->item('base_url').$row->path.'" alt="">
                 <div class="preview-buttons valign-middle">
                     <div class="valign-middle-content">
                         <a class="button size-2 style-2" href="#">
                         <span class="button-wrapper">
                               <span class="icon fa fa-arrow-right" style="color:white"></span>
                         <span class="text">lihat detail</span>
                         </span>
                         </a>
                         <a class="button size-2 style-3" href="#">
                         <span class="button-wrapper">
                               <span class="icon fa fa-legal"></span>
                         <span class="text">mulai bid</span>
                         </span>
                         </a>
                     </div>
                 </div>
             </div>
             <div class="title">
                 <div class="simple-article size-1 color col-xs-b5"><a href="#">'.$row->nama_barang.'</a></div>
                 <div class="h6 animate-to-green"><a href="#">modern beat ht</a></div>
             </div>
             <div class="description">
                 <div class="simple-article text size-2">'.$row->deskripsi.'</div>
             </div>
             <div class="price">
                 <div class="simple-article size-4"><span class="dark">RP '.$row->harga_max.'</span></div>
             </div>
         </div>
        </div>';
     }
     return $value;
 }
 function GetSnipItemModel()
 {
     $barang="SELECT k.id_kategori,k.nama `nama_kategori`,i.id_item `id_barang`,i.nama `nama_barang`,i.keterangan `deskripsi`,concat(k.path,k.id_kategori,'/',i.id_Item,'/',im.namepath) `path`,i.harga_max,i.harga_min,i.tgl_akhir
        from kategori k,item i, (select max(namepath)`namepath`,id_item from item_image im group by id_item) im
        where k.id_kategori = i.id_kategori
        and im.id_item = i.id_item";
     $value="";

     foreach ($this->db->query($barang)->result() as $row)
     {
         $value.='<div class="swiper-slide">
         <div class="product-shortcode style-5 small">
         <div style="border: 1px #eee solid; padding: 5px; width:250px; height:68px;">
         <div class="product-label green">SISA WAKTU : </div>
         <div class="countdown" data-end="'.$row->tgl_akhir.'"></div>
         </div>
         <div class="preview">
             <div class="swiper-container" data-loop="1" data-touch="0">
                 <div class="swiper-button-prev style-1"></div>
                 <div class="swiper-button-next style-1"></div>
                 <div class="swiper-wrapper">
                     <div class="swiper-slide">
                         <img style="width:150px;height:150px;" src="'.$this->config->item('base_url').$row->path.'" alt="" />
                     </div>
                     <div class="swiper-slide">
                         <img style="width:150px;height:150px;" src="'.$this->config->item('base_url').$row->path.'" alt="" />
                     </div>
                 </div>
             </div>
         </div>
         <div class="title">
             <div class="simple-article size-1 color col-xs-b5"><a href="#">'.$row->nama_kategori.'</a></div>
             <div class="h6 animate-to-green"><a href="#">'.$row->nama_barang.'</a></div>
         </div>
         <div class="description">
             <div class="simple-article text size-2">'.$row->deskripsi.'</div>
         </div>
         <div class="price">
             <div class="simple-article size-4 dark">RP '.$row->harga_max.'</div>
         </div>

         <div class="preview-buttons">
             <div class="buttons-wrapper">
                 <a class="button size-2 style-2" href="#">
                 <span class="button-wrapper">
                       <span class="icon fa fa-arrow-right"></span>
                 <span class="text">mulai bid</span>
                 </span>
                 </a>
                 <a class="button size-2 style-3" href="#">
                 <span class="button-wrapper">
                       <span class="icon fa fa-legal"></span>
                 <span class="text">mulai bid</span>
                 </span>
                 </a>
             </div>
         </div>
     </div>
 </div>';
     }
     return $value;
 }
}
