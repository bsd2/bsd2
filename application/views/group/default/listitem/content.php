<div class="container">
  <div class="empty-space col-xs-b15 col-sm-b30"></div>
  <?php $this->load->view('group/default/includes/breadcrumps');?>
    <div class="empty-space col-xs-b15 col-sm-b50"></div>
  <div class="row">
    <div class="col-md-12">
      <div class="align-inline spacing-1">
        <div class="h4"><?php if($this->uri->segment(2)==null) {echo "All Kategori";} else {echo $this->uri->segment(2);}?></div>
      </div>
      <div class="align-inline spacing-1 hidden-xs">
        <a class="pagination toggle-products-view active"><span class="fa fa-th"></span></a>
        <a class="pagination toggle-products-view"><span class="fa fa-list-ul"></span></a>
      </div>
      <div class="align-inline spacing-1 filtration-cell-width-1">
        <select class="SlectBox small">
          <option disabled="disabled" selected="selected">Daftar Katalog</option>
        </select>
      </div>
      <div class="empty-space col-xs-b25 col-sm-b60"></div>

      <div class="products-content">
        <div class="products-wrapper">
          <div class="row nopadding">

          </div>
        </div>
      </div>
      <div class="empty-space col-xs-b35 col-sm-b0"></div>
      <div class="row">
        <div class="col-sm-3 hidden-xs">
          <button class="button size-1 style-5" id="prev">
            <span class="button-wrapper">
      <span class="icon"><i class="fa fa-angle-left" aria-hidden="true"></i></span>
            <span class="text">prev page</span>
            </span>
          </button>
        </div>
        <div class="col-sm-6 text-center">
          <div class="pagination-wrapper">
          </div>
        </div>
        <div class="col-sm-3 hidden-xs text-right">
          <button class="button size-1 style-5" id="next">
            <span class="button-wrapper">
      <span class="icon"><i class="fa fa-angle-right" aria-hidden="true"></i></span>
            <span class="text">next page</span>
            </span>
          </button>
        </div>
      </div>

      <div class="empty-space col-xs-b35 col-md-b70"></div>
      <div class="empty-space col-md-b70"></div>
    </div>

  </div>
</div>
<script>
  // $(document).ready(function(){
    $.ajax({

      url: '<?= $this->config->item('base_url').'HomePage/GetKatalog';?>',
      dataType: 'text',
      success: function (respon)
      {
        var respon = jQuery.parseJSON(respon);
        $.each(respon, function(val, text) {
            $('select.SlectBox')[0].sumo.add(text['id_kategori'],text['nama'],0);

        });
        $('select').on('change',function(){
          var page ='<?= $this->config->item('base_url').'listitem/'?>';
          var redirect = page+this.value;
          window.location.href=redirect;
        });
      }
    });

    $.ajax({

      url: '<?= $this->config->item('base_url').'HomePage/GetKatalogCount';?>',
      data : {'kategori':'<?=$this->uri->segment(2);?>'},
      dataType: 'text',
      type: "POST",
      success: function (respon)
      {
        stats='active';
        for (var i = 1; i <= respon; i++) {
          $('.pagination-wrapper').html('<a class="pagination '+stats+'">'+i+'</a>')
          stats='';
        }
            request($('.pagination.active').text());
      }
    });


    $(document).on('click', '.pagination-wrapper .pagination', function() {
      $(this).parent().find('.pagination').removeClass('active');
      $(this).addClass('active');
      request($('.pagination.active').text());
    });

    $('#prev').on('click', function() {
      // alert($('.pagination.active').prev().length);
      // exit();
      if($('.pagination.active').prev().length>0)
      {
        $('.pagination.active').removeClass('active').prev().addClass('active');
        request($('.pagination.active').text());
      }
      else {
        // alert('');
      }
    });

    $('#next').on('click', function() {
      // alert($('.pagination.active').next().length);
      // exit()
      if($('.pagination.active').next().length>1)
      {
        $('.pagination.active').removeClass('active').next().addClass('active');
        request($('.pagination.active').text());
      }
      else {
        // alert('');
      }
    });
    function request(page)
    {
      // alert(page);
      $.ajax({
        url: '<?= $this->config->item('base_url').'HomePage/GetListKatalog/'?>',
        data : {'page':page,'kategori':'<?=$this->uri->segment(2);?>'},
        dataType: 'text',
        type: "POST",
        success: function (respon)
        {
          // alert(respon);
          $('.nopadding').html(respon);
          _functions.initCounter();
          $('.itms').on('click',function(){
            // addtocart($(this).attr("aidi"),$(this).attr("price"));
            callpopupbid($(this).attr("aidi"));
            // alert($(this).attr("aidi"));
          })
        }
      });
    }


  // });
</script>
