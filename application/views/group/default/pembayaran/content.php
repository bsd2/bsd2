<div class="container">
  <div class="empty-space col-xs-b15 col-sm-b30"></div>
  <?php $this->load->view('group/default/includes/breadcrumps');?>
    <div class="empty-space col-xs-b15 col-sm-b50"></div>
    <div class="row">
        <div class="col-md-12">
            <h4 class="h4 col-xs-b25">your order</h4>
            <?php
            $val='';
            foreach ($data['detailnya'] as $key => $value)
              { $val.='<div class="cart-entry clearfix">
                  <a class="cart-entry-thumbnail" href="#"><img style="width:80px;height:80px" src="'.$value['truepath'].'" alt=""></a>
                  <div class="cart-entry-description">
                      <table>
                          <tbody>
                              <tr>
                                  <td>
                                      <div class="h6"><a href="#">modern beat ht</a></div>
                                      <div class="simple-article size-1">QUANTITY: '.$value['qty'].'</div>
                                  </td>
                                  <td>
                                      <div class="simple-article size-3 grey">'.$value['price'].'</div>
                                      <div class="simple-article size-1">TOTAL: '.$value['subtotal'].'</div>
                                  </td>
                              </tr>
                          </tbody>
                      </table>
                  </div>
              </div>';
              }
              echo $val;
              ?>

            <div class="order-details-entry simple-article size-3 grey uppercase">
                <div class="row">
                    <div class="col-xs-6">
                        cart subtotal
                    </div>
                    <div class="col-xs-6 col-xs-text-right">
                        <div class="color">  <?=$data['totalbayar']?></div>
                    </div>
                </div>
            </div>
            <div class="order-details-entry simple-article size-3 grey uppercase">
                <div class="row">
                    <div class="col-xs-6">
                        shipping and handling
                    </div>
                    <div class="col-xs-6 col-xs-text-right">
                        <div class="color">free shipping</div>
                    </div>
                </div>
            </div>
            <div class="order-details-entry simple-article size-3 grey uppercase">
                <div class="row">
                    <div class="col-xs-6">
                        order total
                    </div>
                    <div class="col-xs-6 col-xs-text-right">
                        <div class="color"><?=$data['totalbayar']?></div>
                    </div>
                </div>
            </div>
            <div class="empty-space col-xs-b50"></div>
            <h4 class="h4 col-xs-b25">payment method</h4>
            <select class="SlectBox">
                <option selected="selected">PayPal</option>
                <option selected="selected">Visa</option>
                <option selected="selected">Master Card</option>
            </select>
            <div class="empty-space col-xs-b10"></div>
            <div class="simple-article size-2">* Barang yang sudah dibeli tidak dapat dikembalikan.</div>
            <div class="empty-space col-xs-b30"></div>
            <div class="button block size-2 style-3">
                <span class="button-wrapper">
                    <span class="icon" style="color:white;"><i class="fa fa-credit-card-alt"></i></span>
                    <span class="text">place order</span>
                </span>
                <input type="submit"/>
            </div>
        </div>
    </div>
</div>
<div class="empty-space col-xs-b35 col-md-b70"></div>


<script>

</script>
