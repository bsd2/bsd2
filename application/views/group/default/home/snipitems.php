<div class="container">
<div class="text-center">
<div class="h2">snip item</div>
<div class="title-underline center"><span></span></div>
</div>
</div>
<div class="tabs-block">
<div class="tab-entry visible">
<div class="slider-wrapper side-borders">
<div class="swiper-button-prev hidden-xs hidden-sm"></div>
<div class="swiper-button-next hidden-xs hidden-sm"></div>
<div class="swiper-container" data-breakpoints="1" data-xs-slides="1" data-sm-slides="2" data-md-slides="3" data-lt-slides="4"  data-slides-per-view="5">
    <div class="swiper-wrapper" id="snipitem">
        <!-- <div class="swiper-slide">
            <div class="product-shortcode style-5 small">
                <div class="product-label green">best price</div>
                <div class="icons">
                    <a class="entry"><i class="fa fa-check" aria-hidden="true"></i></a>
                    <a class="entry open-popup" data-rel="3"><i class="fa fa-eye" aria-hidden="true"></i></a>
                    <a class="entry"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
                </div>
                <div class="preview">
                    <div class="swiper-container" data-loop="1" data-touch="0">
                        <div class="swiper-button-prev style-1"></div>
                        <div class="swiper-button-next style-1"></div>
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <<img src="<?= $this->config->item('base_url')?>assets/img/product-66.jpg" alt="" />
                            </div>
                            <div class="swiper-slide">
                                <<img src="<?= $this->config->item('base_url')?>assets/img/product-67.jpg" alt="" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="title">
                    <div class="simple-article size-1 color col-xs-b5"><a href="#">Modern edition</a></div>
                    <div class="h6 animate-to-green"><a href="#">modern beat nine</a></div>
                </div>
                <div class="description">
                    <div class="simple-article text size-2">Mollis nec consequat at In feugiat molestie tortor a malesuada</div>
                </div>
                <div class="price">
                    <div class="simple-article size-4 dark">$630.00</div>
                </div>

                <div class="preview-buttons">
                    <div class="buttons-wrapper">
                        <a class="button size-2 style-2" href="#">
                            <span class="button-wrapper">
                                <span class="icon"><<img src="<?= $this->config->item('base_url')?>assets/img/icon-1.png" alt=""></span>
                                <span class="text">Learn More</span>
                            </span>
                        </a>
                        <a class="button size-2 style-3" href="#">
                            <span class="button-wrapper">
                                <span class="icon"><<img src="<?= $this->config->item('base_url')?>assets/img/icon-3.png" alt=""></span>
                                <span class="text">Add To Cart</span>
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </div> -->
    </div>
    <div class="swiper-pagination relative-pagination visible-xs visible-sm"></div>
</div>
</div>
</div>

</div>
<div class="empty-space col-xs-b30"></div>

<script>
  function hotitem()
  {
  // alert(page);
  $.ajax({
  url: '<?= $this->config->item('base_url').'HomePage/GetSnipItem/'?>',
  dataType: 'text',
  type: "POST",
  success: function (respon)
    {
      $('#snipitem').html(respon);
      _functions.initCounter();
      // _functions.pageCalculations();
      //
  		// 	_functions.resizeCall();
    }
  });
  }
hotitem();
</script>
