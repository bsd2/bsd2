<style type="text/css">
body, html {
  height: 100%;
}
.parallax {
   height: inherit;
   background-attachment: fixed;
   background-position: center;
   background-repeat: no-repeat;
   background-size: cover;
}
.parallax.main1 {
   background-image: url("<?= $this->config->item('base_url')?>/assets/img/6.jpg");
 }
.parallax.main2 {
  background-image: url("<?= $this->config->item('base_url')?>/assets/img/7.jpg");
}
.parallax.main3 {
   background-image: url("<?= $this->config->item('base_url')?>/assets/img/8.jpg");
 }
</style>
<div class="slider-wrapper">
<div class="swiper-container" data-parallax="1" data-auto-height="1" data-autoplay="2000">
   <div class="swiper-wrapper">
       <div class="swiper-slide"  style="background-image: url(<?= $this->config->item('base_url')?>assets/img/background-1.jpg);">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="cell-view page-height">
                            <div class="col-xs-b40 col-sm-b80"></div>

                            <div class="col-xs-b40 col-sm-b80"></div>
                        </div>
                    </div>
                </div>
                <div class="slider-product-preview" data-swiper-parallax-x="-600">

                </div>
                <div class="empty-space col-xs-b80 col-sm-b0"></div>
            </div>
       </div>
       <div class="swiper-slide"  style="background-image: url(<?= $this->config->item('base_url')?>assets/img/background-2.jpg);">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="cell-view page-height">
                            <div class="col-xs-b40 col-sm-b80"></div>

                            <div class="col-xs-b40 col-sm-b80"></div>
                        </div>
                    </div>
                </div>
                <div class="slider-product-preview" data-swiper-parallax-x="-600">

                </div>
                <div class="empty-space col-xs-b80 col-sm-b0"></div>
            </div>
       </div>
       <div class="swiper-slide"  style="background-image: url(<?= $this->config->item('base_url')?>assets/img/background-3.jpg);">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="cell-view page-height">
                            <div class="col-xs-b40 col-sm-b80"></div>

                            <div class="col-xs-b40 col-sm-b80"></div>
                        </div>
                    </div>
                </div>
                <div class="slider-product-preview" data-swiper-parallax-x="-600">

                </div>
                <div class="empty-space col-xs-b80 col-sm-b0"></div>
            </div>
       </div>
   </div>
   <div class="swiper-pagination swiper-pagination-white"></div>
</div>
</div>
