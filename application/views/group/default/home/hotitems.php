<div class="container">
<div class="text-center">
<div class="h2">hot items</div>
<div class="title-underline center"><span></span></div>
</div>
</div>
<div class="slider-wrapper">
    <div class="swiper-button-prev visible-lg"></div>
    <div class="swiper-button-next visible-lg"></div>
    <div class="swiper-container" data-breakpoints="1" data-xs-slides="1" data-sm-slides="2" data-md-slides="2" data-lt-slides="3"  data-slides-per-view="4">
        <div class="swiper-wrapper" id="hotitem">
            <!-- <div class="swiper-slide">
                <div class="product-shortcode style-1 big">
                    <div class="product-label red">best price</div>
                    <div class="preview">
                        <img src="<?= $this->config->item('base_url')?>/assets/img/product-62.jpg" alt="">
                        <div class="preview-buttons valign-middle">
                            <div class="valign-middle-content">
                                <a class="button size-2 style-2" href="#">
                                    <span class="button-wrapper">
                                        <span class="icon"><img src="<?= $this->config->item('base_url')?>/assets/img/icon-1.png" alt=""></span>
                                        <span class="text">Learn More</span>
                                    </span>
                                </a>
                                <a class="button size-2 style-3" href="#">
                                    <span class="button-wrapper">
                                        <span class="icon"><img src="<?= $this->config->item('base_url')?>/assets/img/icon-3.png" alt=""></span>
                                        <span class="text">Add To Cart</span>
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="title">
                        <div class="simple-article size-1 color col-xs-b5"><a href="#">MODERN EDITION</a></div>
                        <div class="h6 animate-to-green"><a href="#">modern beat ht</a></div>
                    </div>
                    <div class="description">
                        <div class="simple-article text size-2">Mollis nec consequat at In feugiat molestie tortor a malesuada etiam a venenatis ipsum</div>
                        <div class="icons">
                            <a class="entry"><i class="fa fa-check" aria-hidden="true"></i></a>
                            <a class="entry open-popup" data-rel="3"><i class="fa fa-eye" aria-hidden="true"></i></a>
                            <a class="entry"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
                        </div>
                    </div>
                    <div class="price">
                        <div class="color-selection">
                            <div class="entry active" style="color: #a7f050;"></div>
                            <div class="entry" style="color: #50e3f0;"></div>
                            <div class="entry" style="color: #eee;"></div>
                        </div>
                        <div class="simple-article size-4"><span class="dark">$155.00</span></div>
                    </div>
                </div>
            </div> -->
        </div>
        <div class="swiper-pagination relative-pagination visible-xs visible-sm"></div>
    </div>
</div>
<script>
  function hotitem()
  {
  // alert(page);
  $.ajax({
  url: '<?= $this->config->item('base_url').'HomePage/GetHotItem/'?>',
  dataType: 'text',
  type: "POST",
  success: function (respon)
    {
      $('#hotitem').html(respon);
      _functions.initCounter();
    }
  });
  }
hotitem();
</script>
