<div class="row nopadding">
    <div class="col-sm-6">
        <div class="banner-shortcode style-3 wide" style="background-image: url(<?= $this->config->item('base_url')?>/assets/img/background-9.jpg);">
            <div class="valign-middle-cell">
                <div class="valign-middle-content">
                    <div class="simple-article size-3 light transparent uppercase col-xs-b5">online bidding</div>
                    <h3 class="h3 light">OBID</h3>
                    <div class="title-underline left"><span></span></div>
                    <div class="simple-article size-4 light transparent col-xs-b30">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesentir pulvinar ante et nisl scelerisque.</div>
                    <a class="button size-2 style-1" href="#">
                        <span class="button-wrapper">
                            <span class="icon"><img src="<?= $this->config->item('base_url')?>/assets/img/icon-1.png" alt=""></span>
                            <span class="text">info</span>
                        </span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="banner-shortcode style-3 wide" style="background-image: url(<?= $this->config->item('base_url')?>/assets/img/background-10.jpg);">
            <div class="valign-middle-cell">
                <div class="valign-middle-content">
                    <div class="simple-article size-3 light transparent uppercase col-xs-b5">online bidding</div>
                    <h3 class="h3 light">OBID</h3>
                    <div class="title-underline left"><span></span></div>
                    <div class="simple-article size-4 light transparent col-xs-b30">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesentir pulvinar ante et nisl scelerisque.</div>
                    <a class="button size-2 style-1" href="#">
                        <span class="button-wrapper">
                            <span class="icon"><img src="<?= $this->config->item('base_url')?>/assets/img/icon-1.png" alt=""></span>
                            <span class="text">info</span>
                        </span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
