<div class="container">
    <div class="empty-space col-xs-b15 col-sm-b30"></div>
    <?php $this->load->view('group/default/includes/breadcrumps');?>

    <div class="empty-space col-xs-b15 col-sm-b50"></div>
    <div class="row">
        <div class="col-md-12 ">
            <div class="row">
                <div class="col-sm-6 col-xs-b30 col-sm-b0">
                    <div class="main-product-slider-wrapper swipers-couple-wrapper">
                        <div class="swiper-container swiper-control-top">
                           <div class="swiper-button-prev hidden"></div>
                           <div class="swiper-button-next hidden"></div>
                           <div class="swiper-wrapper">
                             <?php
                             $val='';
                             foreach ($data['detailnya'] as $key => $value)

                               { $val.=
                                   '<div class="swiper-slide">
                                    <div class="swiper-lazy-preloader"></div>
                                    <div class="product-big-preview-entry swiper-lazy" data-background="'.$this->config->item('base_url').$value['src'].'"></div>
                                    </div>';
                               }
                               echo $val;
                               ?>
                           </div>
                        </div>
                        <div class="empty-space col-xs-b30 col-sm-b60"></div>
                        <div class="swiper-container swiper-control-bottom" data-breakpoints="1" data-xs-slides="3" data-sm-slides="3" data-md-slides="4" data-lt-slides="4" data-slides-per-view="5" data-center="1" data-click="1">
                           <div class="swiper-button-prev hidden"></div>
                           <div class="swiper-button-next hidden"></div>
                           <div class="swiper-wrapper">
                            <?php
                            $val='';
                            foreach ($data['detailnya'] as $key => $value)

                             { $val.=
                                 '<div class="swiper-slide">
                                      <div class="product-small-preview-entry">
                                          <img width="84px" height="84px" src="'.$this->config->item('base_url').$value['src'].'" alt="" />
                                      </div>
                                  </div>';
                             }
                             echo $val;
                             ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="simple-article size-3 grey col-xs-b5"><?=$data['nama_kategori'];?></div>
                    <div class="h3 col-xs-b25"><?=$data['nama_item'];?></div>
                    <div class="row col-xs-b25">
                        <div class="col-sm-6">
                            <div class="simple-article size-5 grey">CURRENT PRICE: <span class="color"><?=$data['harga_max'];?></span></div>
                        </div>
                        <div class="col-sm-6 col-sm-text-right">
                            <div class="rate-wrapper align-inline">
                                <?php
                                $val='';
                                $tmp=$data['star'];
                                for ($i=0;$i<5;$i++)
                                {
                                    if($tmp>0)
                                    {
                                        $val.='<i class="fa fa-star" aria-hidden="true"></i>';
                                    }else {
                                        $val.='<i class="fa fa-star-o" aria-hidden="true"></i>';
                                    }
                                    $tmp--;
                                }
                                 echo $val;
                                 ?>
                            </div>
                            <div class="simple-article size-2 align-inline"><?=$data['reviews'];?> Reviews</div>
                        </div>
                    </div>
                    <div class="simple-article size-3 col-xs-b30"><?=$data['keterangan'];?></div>
                    <div class="empty-space col-xs-b35 col-md-b50"></div>
                    <div style="border: 1px #eee solid; padding: 5px;">
                    <div class="simple-article size-1 col-xs-b5">SISA WAKTU : </div>
                    <div class="countdown" data-end="<?=$data['tgl_akhir'];?>"></div>
                    </div>
                    <div class="empty-space col-xs-b35 col-md-b120"></div>

                    <div class="row m5 col-xs-b40">
                        <div class="col-sm-6 col-xs-b10 col-sm-b0">
                            <a class="button size-2 style-2 block open-popup bid" data-rel="3" id="bidnow" >
                                <span class="button-wrapper">
                                    <span class="icon" style="color:white;"><i class="fa fa-legal"></i></span>
                                    <span class="text">bid sekarang</span>
                                </span>
                            </a>
                        </div>
                        <div class="col-sm-6">
                            <a class="button size-2 style-1 block noshadow" href="#">
                            <span class="button-wrapper">
                                <span class="icon"><i class="fa fa-star-o" aria-hidden="true"></i></span>
                                <span class="text">give rating</span>
                            </span>
                        </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="empty-space col-xs-b35 col-md-b70"></div>
        </div>
    </div>
</div>
<script>
$('#bidnow').on('click',function(){
    callpopupbid('<?=$data['id_item'];?>');
});

</script>
