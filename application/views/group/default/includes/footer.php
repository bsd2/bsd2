<footer>
  <div class="container">
    <div class="footer-top">
      <div class="row">
        <div class="col-sm-6 col-md-12">
          <h6 class="h6 light">about</h6>
          <div class="empty-space col-xs-b20"></div>
          <img width="50px" src="<?= $this->config->item('base_url')?>/assets/img/logo-2.png" alt="" />
          <div class="empty-space col-xs-b20"></div>
          <div class="simple-article size-2 light fulltransparent">Integer posuere orci sit amet feugiat pellent que. Suspendisse vel tempor justo, sit amet posuere orci dapibus auctor</div>
          <div class="empty-space col-xs-b20"></div>
          <div class="footer-contact"><i class="fa fa-mobile" aria-hidden="true"></i> contact us:
            <a href="tel:+1234567890"></a>
          </div>
          <div class="footer-contact"><i class="fa fa-envelope-o" aria-hidden="true"></i> email: <a href="mailto:dian.yulius@gmail.com">dian.yulius@gmail.com</a></div>
        </div>
        

      </div>
    </div>
    <div class="footer-bottom">
      <div class="row">
        <div class="col-lg-8 col-xs-text-center col-lg-text-left col-xs-b20 col-lg-b0">
          <div class="copyright">&copy; 2016 All rights reserved. Development by OBID</div>
        </div>
      </div>
    </div>
</footer>
