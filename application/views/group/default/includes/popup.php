<div class="popup-wrapper">
  <div class="bg-layer"></div>
  <div class="popup-content" data-rel="1">
    <div class="layer-close"></div>
    <div class="popup-container size-1">
      <div class="popup-align">
        <h3 class="h3 text-center">Log in</h3>
        <div class="empty-space col-xs-b30"></div>
        <input class="simple-input" id="emaillogin" type="email" value="" placeholder="Your email" />
        <div class="empty-space col-xs-b10 col-sm-b20"></div>
        <input class="simple-input" id="passlogin" type="password" value="" placeholder="Enter password" />
        <div class="empty-space col-xs-b10 col-sm-b20"></div>

        <div class="row">
            <div class="col-sm-6 col-xs-b10 col-sm-b0">
                <div class="empty-space col-xs-b5"></div>
                <a class="simple-link" id="regnow">register now</a>
            </div>
          <div class="col-sm-6 text-right">
            <button class="button size-2 style-3" id="login">
              <span class="button-wrapper">
                                <span class="icon"><img src="<?= $this->config->item('base_url')?>/assets/img/icon-4.png" alt="" /></span>
              <span class="text">submit</span>
              </span>
            </button>
          </div>
        </div>
      </div>
      <div class="button-close"></div>
    </div>
  </div>

  <div class="popup-content" data-rel="2">
    <div class="layer-close"></div>
    <div class="popup-container size-1">
      <div class="popup-align">
        <h3 class="h3 text-center">register</h3>
        <div class="empty-space col-xs-b30"></div>
        <input class="simple-input" id="nameregister" type="text" placeholder="Your name" />
        <div class="empty-space col-xs-b10 col-sm-b20"></div>
        <input class="simple-input" id="emailregister" type="email"  placeholder="Your email" />
        <div class="empty-space col-xs-b10 col-sm-b20"></div>
        <input class="simple-input" id="passregister" type="password" placeholder="Enter password" />
        <div class="empty-space col-xs-b10 col-sm-b20"></div>
        <!-- <input class="simple-input" type="password" value="" placeholder="Repeat password" />
        <div class="empty-space col-xs-b10 col-sm-b20"></div> -->
        <div class="row">
          <div class="col-sm-7 col-xs-b10 col-sm-b0">
            <div class="empty-space col-sm-b15"></div>
            <label class="checkbox-entry">
              <input type="checkbox" /><span><a href="#">Privacy policy agreement</a></span>
            </label>
          </div>
          <div class="col-sm-5 text-right">
            <button class="button size-2 style-3" id="register">
              <span class="button-wrapper">
                                <span class="icon"><img src="<?= $this->config->item('base_url')?>/assets/img/icon-4.png" alt="" /></span>
              <span class="text">submit</span>
              </span>
            </button>
          </div>
        </div>
        <div class="button-close"></div>
      </div>
    </div>
  </div>

  <div class="popup-content bidmod" data-rel="3">
      <div class="layer-close"></div>
      <div class="popup-container size-2">
          <div class="popup-align">
              <div class="row">
                  <div class="col-sm-6 col-xs-b30 col-sm-b0">
                      <div class="main-product-slider-wrapper swipers-couple-wrapper">
                          <div class="swiper-container">
                             <div class="swiper-wrapper">
                                 <div class="swiper-slide">
                                      <div class="product-big-preview-entry swiper-lazy" id="popupimage" data-background=""></div>
                                 </div>
                             </div>
                          </div>
                      </div>
                  </div>
                  <div class="col-sm-6">
                      <div class="simple-article size-3 grey col-xs-b5" id="popupkategori"></div>
                      <div class="row col-xs-b30">
                      <div class="h3 col-xs-b25" id="popupnama"></div>
                      <div style="border: 1px #eee solid; padding: 5px;">
                      <div class="simple-article size-1 col-xs-b5">SISA WAKTU : </div>
                      <div id="popupbidend" ></div>
                      </div>
                      </div>
                      <div class="row col-xs-b30">
                          <div class="col-sm-6">
                              <div class="simple-article size-5 grey">BUY PRICE: <span class="color" id="popupbuyprice"></span></div>
                          </div>
                          <div class="col-sm-6">
                              <div class="simple-article size-5 grey">CURRENT PRICE: <span class="color" id="popuplastprice"></span></div>
                          </div>
                      </div>
                      <div class="row col-xs-b30">
                          <div class="col-sm-3">
                              <div class="h6 detail-data-title size-1">tawar :</div>
                          </div>
                          <div class="col-sm-9">
                              <input class="simple-input" type="number" placeholder="Masukan Jumlah Tawaran" id="popuppayprice"/>
                          </div>
                      </div>
                        <div class="row">
                          <div class="col-sm-12">
                              <a class="button size-2 style-3 block" href="#">
                              <span class="button-wrapper" id="bidding">
                                  <span class="icon"><i class="fa fa-edit" aria-hidden="true"></i></span>
                                  <span class="text">tawar</span>
                              </span>
                          </a>
                          </div>
                        </div>
                        <div class="popup-or" >
                            <span>atau</span>
                        </div>
                        <div class="row">
                          <div class="col-sm-12">
                              <a class="button size-2 style-2 block" href="#">
                              <span class="button-wrapper" id="forcebuy">
                                  <span class="icon"><i class="fa fa-shopping-bag" style="color:white" aria-hidden="true"></i></span>
                                  <span class="text">langsung beli</span>
                              </span>
                          </a>
                          </div>
                        </div>

                  </div>
              </div>
          </div>
          <div class="button-close"></div>
      </div>
  </div>

</div>
<script>
  $('#login').on('click',function(){
    $.ajax({
      url: '<?= $this->config->item('base_url') ?>HomePage/UserLogin',
      type: "POST",
      data: {emaillogin: $('#emaillogin').val(),passlogin: $('#passlogin').val()},
      cache : false,
      success: function(response)
      {
        alertify.alert(response, function()
        {
          if(response =="LOGGED IN")
          {
            location.reload();
          }
        });
     },
     error: function(xhr, textStatus, errorThrown) {
        console.log(errorThrown);
        console.log(xhr);
        return false;
      }
    });
  });
  $('#register').on('click',function(){
    $.ajax({
      url: '<?= $this->config->item('base_url') ?>HomePage/UserRegister',
      type: "POST",
      data: {emailregister: $('#emailregister').val(),passregister: $('#passregister').val(),nameregister: $('#nameregister').val()},
      cache : false,
      success: function(response)
      {
        alertify.alert(response, function()
        {
          // if(response =="SUKSES")
          // {
            location.reload();
          // }
        });
     },
     error: function(xhr, textStatus, errorThrown) {
        console.log(errorThrown);
        console.log(xhr);
        return false;
      }
    });
  });
$('#regnow').on('click',function(){
  $('.open-popup.rgs').click();
})

function callpopupbid(id)
{
  $.ajax({
    url: '<?= $this->config->item('base_url') ?>HomePage/GetPopUpBid',
    type: "POST",
    data: {'id': id},
    cache : false,
    success: function(response)
    {
      var popup = JSON.parse(response);
      $('#popupkategori').html(popup[0]['nama_kategori']);
      $('#popupnama').html(popup[0]['nama']);
      $('#popupimage').css('background-image', popup[0]['path']);
      $('#popupbuyprice').html(popup[0]['harga_max']);
      $('#popuplastprice').html(popup[0]['jml_bidding']);
      $('#popuppayprice').val(popup[0]['auto_bid']);
      $("#popupbidend").addClass("countdown").attr("data-end",popup[0]['tgl_akhir']);
      $('#forcebuy').attr('val',id);
      $('#bidding').attr('val',id);
  		_functions.initCounter();
   },
   error: function(xhr, textStatus, errorThrown) {
      console.log(errorThrown);
      console.log(xhr);
      return false;
    }
  });
}
$('#forcebuy').on('click',function(){
  addtocart($(this).attr('val'),$('#popupbuyprice').html());
})
$('#bidding').on('click',function(){
  addtobid($(this).attr('val'),$('#popuppayprice').val());
})
function addtocart(id,pric)
{
  if ('<?=$this->session->userdata('useremail')?>'==''){
    $('.open-popup.lgn').click();
  }else {
  $.ajax({
    url: '<?= $this->config->item('base_url').'HomePage/AddToCart/'?>',
    data : {'id':id,'pric':pric},
    dataType: 'text',
    type: "POST",
    success: function (respon)
    {
      alertify.alert(respon, function()
      {
        recart();
      });
    }
  });
  }
}
function addtobid(id,pric)
{
  if ('<?=$this->session->userdata('useremail')?>'==''){
    $('.open-popup.lgn').click();
  }else {
  $.ajax({
    url: '<?= $this->config->item('base_url').'HomePage/AddToBidding/'?>',
    data : {'id':id,'pric':pric},
    dataType: 'text',
    type: "POST",
    success: function (respon)
    {
      alertify.alert(respon, function()
      {
        recart();
      });
    }
  });
  }
}
</script>
