<div class="header-top">
  <div class="content-margins">
    <div class="row">
      <div class="col-md-5 hidden-xs hidden-sm"></div>

      <div class="col-md-7 col-md-text-right">
        <?php if($this->session->userdata('useremail')== null) {
          echo '<div class="entry"><a class="open-popup lgn" data-rel="1"><b>login</b></a>&nbsp; or &nbsp;<a class="open-popup rgs" data-rel="2"><b>register</b></a></div>';
        }
        else {
          echo '
            <div class="entry hidden-xs hidden-sm cart">

          </div>
          <div class="entry language">
              <div class="title"><span class="fa fa-user"></span></div>
                  <div class="language-toggle header-toggle-animation">
                    <a href="'.$this->config->item('base_url').'infouser'.'">user</a>
                    <a href="'.$this->config->item('base_url').'userupload'.'">upload</a>
                    <a id="userlogout">logout</a>

                  </div>
              </div>';
        }

        ?>
        <div class="hamburger-icon">
          <span></span>
          <span></span>
          <span></span>
        </div>
      </div>
    </div>
  </div>
</div>
<script>

$('#userlogout').on('click',function(){
  $.ajax({
    url: '<?= $this->config->item('base_url') ?>HomePage/UserLogout',
    type: "POST",
    cache : false,
    success: function(response)
    {
      location.reload();
   },
   error: function(xhr, textStatus, errorThrown) {
      console.log(errorThrown);
      console.log(xhr);
      return false;
    }
  });
});
recart();
function delcar(itm){
  $.ajax({
    url: '<?= $this->config->item('base_url') ?>HomePage/RemoveFromCart',
    type: "POST",
    data: {'itm':itm},
    cache : false,
    success: function(response)
    {
      alertify.alert(response, function()
      {
        recart();
      });
   },
   error: function(xhr, textStatus, errorThrown) {
      console.log(errorThrown);
      console.log(xhr);
      return false;
    }
  });
}
function recart()
{
  $.ajax({
    url: '<?= $this->config->item('base_url') ?>HomePage/GetFromCart',
    cache : false,
    success: function(response)
    {
      $('.cart').html(response);
      $('.cart-entry-description .button-close').on('click', function(){
      	if($(this).closest('.cart-overflow').find('.cart-entry').length==1)
        {
          $(this).closest('.cart-entry').replaceWith('<h4 class="h4">Your shopping cart is empty</h4>');
        }
      	else {
          $(this).closest('.cart-entry').remove();
        }

      });
      $('.clci').on('click',function(){
        delcar($(this).attr("itm"));
      });
   },
   error: function(xhr, textStatus, errorThrown) {
      console.log(errorThrown);
      console.log(xhr);
      return false;
    }
  });
};
</script>
