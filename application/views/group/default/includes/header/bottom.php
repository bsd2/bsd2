<div class="header-bottom">
  <div class="content-margins">
    <div class="row">
      <div class="col-xs-3 col-sm-1">
        <a id="logo" href="<?= $this->config->item('base_url')?>"><img src="<?= $this->config->item('base_url')?>assets/img/logo-2.png" alt="" /></a>
      </div>
      <div class="col-xs-9 col-sm-11 text-right">
        <div class="nav-wrapper">
          <div class="nav-close-layer"></div>
          <nav>
            <ul>
              <li class="<?php echo ("home" == $this->uri->segment(1) or ""==$this->uri->segment(1)) ? 'active' : '';?>"><a href="<?= $this->config->item('base_url')?>home">home</a></li>
              <li class="<?php echo ("listitem" == $this->uri->segment(1)) ? 'active' : '';?>"><a href="<?= $this->config->item('base_url')?>listitem">listitem</a></li>
              <li class="<?php echo ("pembayaran" == $this->uri->segment(1)) ? 'active' : '';?>"><a href="<?= $this->config->item('base_url')?>pembayaran">pembayaran</a></li>
            </ul>
            <div class="navigation-title">
              Navigation
              <div class="hamburger-icon active">
                <span></span>
                <span></span>
                <span></span>
              </div>
            </div>
          </nav>
        </div>
        <div class="header-bottom-icon toggle-search"><i class ="fa fa-search" aria-hidden="true"></i></div>
        <!-- <div class="header-bottom-icon visible-rd"><i class="fa fa-heart-o" aria-hidden="true"></i></div>
        <div class="header-bottom-icon visible-rd">
          <i class="fa fa-shopping-bag" aria-hidden="true"></i>
          <span class="cart-label">5</span>
        </div> -->
      </div>
    </div>
    <div class="header-search-wrapper">
      <div class="header-search-content">
        <div class="container-fluid">
          <div class="row col-xs-b25">
            <div class="col-sm-8 col-sm-offset-2 col-lg-6 col-lg-offset-3">
              <div class="search-submit">
                <a id="cari">
                  <i class="fa fa-search" aria-hidden="true"></i>
                </a>
              </div>
              <input id="inputsearchbar" class="simple-input style-1" type="text" value="" placeholder="Enter keyword" />
            </div>
          </div>
        </div>
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-8 col-sm-offset-2 col-lg-6 col-lg-offset-3">
              <div class="result-search-popup-overflow">
                <!-- <div class="row col-xs-b25">
                    <h4>adsadasdadas</h4>';
                </div> -->
              </div>
            </div>
          </div>
        </div>
        <div class="button-close"></div>
      </div>
    </div>
  </div>
</div>

<script>
var delayTimer;
  $('#inputsearchbar').bind('keyup', function(e) {
    if (this.value.length > 2) {
      clearTimeout(delayTimer);
      var search = this.value;
      delayTimer = setTimeout(function() {
        requestsearchvalue(search)
      }, 1000);
    }

    function requestsearchvalue(query)
    {
      $.ajax({
        url: '<?= $this->config->item('base_url').'HomePage/SearchQuery/'?>',
        data : {'query':query},
        dataType: 'text',
        type: "POST",
        success: function (respon)
        {
          $('.result-search-popup-overflow').html(respon);
        }
      });
    }
  })
</script>
