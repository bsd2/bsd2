<div class="container">
  <div class="empty-space col-xs-b15 col-sm-b30"></div>
  <?php $this->load->view('group/default/includes/breadcrumps');?>
    <div class="empty-space col-xs-b15 col-sm-b50"></div>

    <div class="row">
        <form id="validation" method="post" role="form">

        <div class="col-md-6 col-xs-b50 col-md-b0">

            <h4 class="h4 col-xs-b25">product data</h4>
            <input class="simple-input" name="product" id="product" type="text" value="" placeholder="NAMA PRODUCT" />

            <div class="empty-space col-xs-b20"></div>
            <select class="SlectBox" id="kategori">
                <option disabled="disabled"  selected="selected">PILIH KATEGORI</option>
            </select>
        </div>
        <div class="col-md-6 col-xs-b50 col-md-b0">
            <h4 class="h4 col-xs-b25">product status</h4>
            <input class="simple-input" type="number" name="hargamin" id="hargamin" value="" placeholder="HARGA MIN" />
            <div class="empty-space col-xs-b20"></div>
            <input class="simple-input" type="number" name="hargamax" id="hargamax" value="" placeholder="HARGA MAX" />
            <div class="empty-space col-xs-b20"></div>
            <!-- <div class='input-group date' id='datetimepicker8'>
                <input type='text' class="simple-input" />
                <span class="input-group-addon">
                    <span class="fa fa-calendar">
                    </span>
                </span>
            </div> -->
            <div class="single-line-form" >
                <input class="simple-input" type="text" id='tgl_akhir' name ="tgl_akhir"  placeholder="Tgl Akhir"/>
                <div class="button size-2 style-3" id="wrap">
                    <span class="button-wrapper">
                        <span class="icon"><span class="fa fa-calendar"></span></span>
                        <span class="text">DATE</span>
                    </span>
                </div>
            </div>

        </div>
        <div class="col-md-12 col-xs-b50 col-md-b0">
          <div class="empty-space col-xs-b20"></div>
          <textarea class="simple-input" name="deskripsi"  id="deskripsi" placeholder="DESKRIPSI"></textarea>
        </div>
        <div class="col-md-12 col-xs-b50 col-md-b0">
          <div class="empty-space col-xs-b20"></div>
            <h4 class="h4 col-xs-b25">upload gambar di sini</h4>
            <div class="dropzone" id="my-dropzone" name="mainFileUploader">
                <div class="fallback">
                    <input name="file" type="file" multiple />
                </div>
            </div>
            <div class="empty-space col-xs-b20"></div>
            <div class="button size-2 style-2">
                <span class="button-wrapper">
                    <span class="icon">
                      <span class="fa fa-check" style="color:white"></span></span>
                    <span class="text">daftar</span>
                </span>
                <input type="submit"/>
            </div>
        </div>
        </form>
    </div>
    <div class="empty-space col-xs-b35 col-md-b70"></div>
</div>
<script>
$('#tgl_akhir').datetimepicker({
    icons: {
        time: "fa fa-clock-o",
        date: "fa fa-calendar",
        up: "fa fa-arrow-up",
        down: "fa fa-arrow-down"
    }
});

$('#wrap').on('click',function(){
  $('#tgl_akhir').focus();
})
$.ajax({
  url: '<?= $this->config->item('base_url').'HomePage/GetKatalog';?>',
  dataType: 'text',
  success: function (respon)
  {
    var respon = jQuery.parseJSON(respon);
    $.each(respon, function(val, text) {
        $('select.SlectBox')[0].sumo.add(text['id_kategori'],text['nama'],0);
        // alert(text['id_kategori']+text['nama']);

    });

  }
});

var wrapperThis;
Dropzone.options.myDropzone = {
    url:'<?= $this->config->item('base_url') ?>HomePage/AddNewItem/',
    dataType: 'text',
    autoProcessQueue: false,
    uploadMultiple: true,
    parallelUploads: 10,
    maxFiles: 10,
    maxFilesize: 3,
    method:"post",
    paramName:"userfile",
    init: function () {
        wrapperThis = this;
        this.on("addedfile", function (file) {
        });
        this.on("sendingmultiple", function(file, xhr, formData) {
        formData.append('product', $('#product').val());
        formData.append('kategori', $( "#kategori option:selected" ).val());
        formData.append('deskripsi', $('#deskripsi').val());
        formData.append('hargamin', $('#hargamin').val());
        formData.append('hargamax', $('#hargamax').val());
        formData.append('tgl_akhir', $('#tgl_akhir').val());
      });
      this.on("successmultiple", function(files, response) {
      alertify.alert(response, function() {
        alertify.message('OK');
        location.reload();
      });
    });
    this.on("errormultiple", function(files, response) {
    });}
};
var wizard = $("#validation").validate({
ignore: [],
rules: {
    product:{
        required: true,
        minlength: 5
    },
    hargamin: {
        required: true,
        max : function(element){
          return $('#hargamax').val();
        }
    },
    deskripsi: {
      required:true,
        minlength: 5
    },
    hargamax :{
        required: true,
        min : function(element){
          return $('#hargamin').val();
        }
    },
    tgl_akhir :{
      required:true
    }
    },
    submitHandler: function(form, event) {
      event.preventDefault();
      wrapperThis.processQueue();
    }
});
</script>
