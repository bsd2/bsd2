<div class="block-entry fixed-background" style="background-image: url(<?= $this->config->item('base_url')?>/assets/img/store.jpg);">
<div class="container">
<div class="row">
    <div class="col-sm-6 col-sm-offset-3">
        <div class="cell-view simple-banner-height text-center">
            <div class="empty-space col-xs-b35 col-sm-b70"></div>
            <h1 class="h1 light">Minly Store</h1>
            <div class="title-underline center"><span></span></div>
            <div class="simple-article light transparent size-4">selamat datang di store kami kami menjual sepatu handmade & import retail & D.ship buka hari kerja hari minggu hanya untuk boking barang </div>
            <div class="empty-space col-xs-b35 col-sm-b70"></div>
        </div>
    </div>
</div>
</div>
</div>
<div class="container">
    <div class="empty-space col-xs-b15 col-sm-b30"></div>
    <?php $this->load->view('group/default/includes/breadcrumps');?>
    <div class="empty-space col-xs-b15 col-sm-b50"></div>
    <div class="container">
    <div class="text-center">
    <div class="simple-article size-3 grey uppercase col-xs-b5">our contacts</div>
    <div class="h2">we ready for your questions</div>
    <div class="title-underline center"><span></span></div>
    </div>
    </div>

    <div class="empty-space col-sm-b15 col-md-b50"></div>

    <div class="container">
    <div class="row">
    <div class="col-sm-3">
        <div class="icon-description-shortcode style-1">
            <img class="icon" src="<?= $this->config->item('base_url')?>/assets/img/icon-25.png" alt="">
            <div class="title h6">Lokasi</div>
            <div class="description simple-article size-2">Belum ada Lokasi</div>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="icon-description-shortcode style-1">
            <img class="icon" src="<?= $this->config->item('base_url')?>/assets/img/icon-23.png" alt="">
            <div class="title h6">Telephone</div>
            <div class="description simple-article size-2" style="line-height: 26px;">
                <a href="tel:+08967892102">  (0896) 789 2102</a>
                <br/>
                <a href="tel:+08967892104">  (0896) 789 2104</a>
            </div>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="icon-description-shortcode style-1">
            <img class="icon" src="<?= $this->config->item('base_url')?>/assets/img/icon-28.png" alt="">
            <div class="title h6">email</div>
            <div class="description simple-article size-2"><a href="mailto:yulius@gmail.com">yulius@gmail.com</a></div>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="icon-description-shortcode style-1">
            <img class="icon" src="<?= $this->config->item('base_url')?>/assets/img/icon-24.png" alt="">
            <div class="title h6">Rate User</div>
            <div class="follow light">
                <a class="entry" href="#"><i class="fa fa-thumbs-o-up"></i> 123</a>
                <a class="entry" href="#"><i class="fa fa-thumbs-o-down"></i> 10</a>
            </div>
        </div>
    </div>
    </div>
    </div>
            <div class="empty-space col-xs-b25 col-sm-b50"></div>
</div>
