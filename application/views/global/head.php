<!doctype html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no, minimal-ui"/>
    <link href="<?= $this->config->item('base_url')?>assets/css/font.apis.css" rel="stylesheet">
    <link href="<?= $this->config->item('base_url')?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= $this->config->item('base_url')?>assets/css/bootstrap.extension.css" rel="stylesheet" type="text/css" />
    <link href="<?= $this->config->item('base_url')?>assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="<?= $this->config->item('base_url')?>assets/css/swiper.css" rel="stylesheet" type="text/css" />
    <link href="<?= $this->config->item('base_url')?>assets/css/sumoselect.css" rel="stylesheet" type="text/css" />
    <link href="<?= $this->config->item('base_url')?>assets/css/alertify.css" rel="stylesheet" type="text/css" />
    <link href="<?= $this->config->item('base_url')?>assets/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= $this->config->item('base_url')?>assets/css/dropzone.css" rel="stylesheet" type="text/css" />
    <link href="<?= $this->config->item('base_url')?>assets/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />

    <script src="<?= $this->config->item('base_url')?>assets/js/jquery-2.2.4.min.js"></script>
    <script src="<?= $this->config->item('base_url')?>assets/js/swiper.jquery.min.js"></script>
    <script src="<?= $this->config->item('base_url')?>assets/js/global.js"></script>
    <script src="<?= $this->config->item('base_url')?>assets/js/jquery.sumoselect.min.js"></script>
    <script src="<?= $this->config->item('base_url')?>assets/js/jquery.classycountdown.js"></script>
    <script src="<?= $this->config->item('base_url')?>assets/js/jquery.knob.js"></script>
    <script src="<?= $this->config->item('base_url')?>assets/js/jquery.throttle.js"></script>
    <script src="<?= $this->config->item('base_url')?>assets/js/scrolltopcontrol.js"></script>
    <script src="<?= $this->config->item('base_url')?>assets/js/alertify.js"></script>
    <script src="<?= $this->config->item('base_url')?>assets/js/dropzone.min.js"></script>
    <script src="<?= $this->config->item('base_url')?>assets/js/jquery.validate.js"></script>
    <script src="<?= $this->config->item('base_url')?>assets/js/moment.js"></script>
    <script src="<?= $this->config->item('base_url')?>assets/js/bootstrap-datetimepicker.min.js"></script>

</head>
